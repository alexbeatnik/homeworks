from os.path import isfile


def search_count_of_line(file_path: str, search_string: str) -> bool:
    result = 0
    with open(f'{file_path}') as file:
        for line in file.readlines():
            if search_string in line.strip():
                result += 1
        print(f'{result}, count of lines in file {file_path}')
        return True


def replase_string(file_path: str, search_string: str, replacement_string: str) -> bool:
    result = 0
    with open(f'{file_path}', 'r') as file:
        filedata = file.read()
        filedata = filedata.replace(search_string, replacement_string)
    with open(f'{file_path}', 'w') as file:
        file.write(filedata)
    file.close()

    print('All lines are changed')
    return True


def algoritm(data_list: list) -> bool:
    way_to_file = data_list[0]
    string_to_serch = data_list[1]

    if len(data_list) == 2:
        search_count_of_line(way_to_file, string_to_serch)
        return True

    if len(data_list) == 3:
        string_to_replase = data_list[2]
        replase_string(way_to_file, string_to_serch, string_to_replase)
        return True


def input_validation(text: str) -> list:
    while True:
        input_data = input(text).split(',')
        if len(input_data) == 2 or len(input_data) == 3:
            if isfile(f'{input_data[0]}'):
                for string in input_data:
                    if not string:
                        break
                return input_data


def main():
    while True:
        input_data = input_validation('To display the number of entered lines in the specified file,'
                                      'enter "File path,search string"\n'
                                      'To replace all occurrences of the specified line, enter '
                                      '"File path, search string, replacement string"\n'
                                      'Please enter data: ')
        algoritm(input_data)
        more = input('Do you want continue: \n')
        if more == 'y' or more == 'Y':
            continue
        else:
            break


if __name__ == '__main__':
    main()
