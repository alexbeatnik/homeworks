from abc import ABC, abstractmethod
from typing import Any
from os.path import isfile


class Input(ABC):

    @abstractmethod
    def value(self) -> Any:
        pass


class TryToInput(Input):

    def __init__(self, inputed_data: str):
        self._inputed_data = inputed_data

    def value(self) -> str:
        return input(self._inputed_data + '\n')


class ListInput(Input):

    def __init__(self, origin: Input):
        self._origin = origin

    def value(self) -> str:
        return self._origin.value().split(',')


class ValidData(ABC):

    @abstractmethod
    def passed(self, value: list) -> bool:
        pass


class ValidLenData(ValidData):

    def passed(self, value: list) -> bool:
        if len(value) == 2 or len(value) == 3:
            return True


class ValidString(ValidData):
    def passed(self, value: list):
        for string in value:
            if not string:
                return False
        return True


class ValidFile(ValidData):

    def passed(self, value: list) -> bool:
        if isfile(f'{value[0]}'):
            return True
        print('File not found')
        return False


class FastFailGate(ValidData):
    def __init__(self, *rules: ValidData):
        self._checks = rules

    def passed(self, value: str) -> bool:
        for rule in self._checks:
            if not rule.passed(value):
                return False
        return True


class ParserCount:

    def __init__(self, file_path: str, search_string: str) -> str:
        self.file_path = file_path
        self.search_string = search_string

    def search_count_of_line(self) -> bool:
        result = 0
        with open(f'{self.file_path}') as file:
            for line in file.readlines():
                if self.search_string in line.strip():
                    result += 1
            print(f'{result}, count of lines in file {self.file_path}')
            return True


class ParserReplase:

    def __init__(self, file_path: str, search_string: str, replacement_string: str):
        self.file_path = file_path
        self.search_string = search_string
        self.replacement_string = replacement_string

    def replase_string(self) -> bool:
        result = 0
        with open(f'{self.file_path}', 'r') as file:
            filedata = file.read()
            filedata = filedata.replace(self.search_string, self.replacement_string)
        with open(f'{self.file_path}', 'w') as file:
            file.write(filedata)

        print('All lines are changed')
        return True

class Algoritm:

    def choose_method(self, data_list: list):

        way_to_file = data_list[0]
        string_to_serch = data_list[1]

        if len(input_data) == 2:
            ParserCount(way_to_file, string_to_serch).search_count_of_line()

        if len(input_data) == 3:
            string_to_replase = data_list[2]
            ParserReplase(way_to_file, string_to_serch, string_to_replase).replase_string()




if __name__ == '__main__':
    while True:
        input_data = ListInput(TryToInput('To display the number of entered lines in the specified file,'
                                      'enter "File path,search string"\n'
                                 'To replace all occurrences of the specified line, enter '
                                '"File path, search string, replacement string"\n'
                                  'Please enter data: ')).value()

        if FastFailGate(ValidLenData(), ValidString(), ValidFile()).passed(input_data):
            Algoritm().choose_method(input_data)
            more = input('Do you want continue: \n')
            if more == 'y' or more == 'Y':
                continue
            else:
                break
        else:
            print('You inputed incorrect data')


