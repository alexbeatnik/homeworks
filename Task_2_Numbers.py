units = {0: 'ноль', 1: 'один', 2: 'два', 3: 'три',
         4: 'четыре', 5: 'пять', 6: 'шесть',
         7: 'семь', 8: 'восемь', 9: 'девять',
         10: 'десять', 11: 'одинадцать', 12: 'двенадцать',
         13: 'тринадцать', 14: 'четырнадцать', 15: 'пятнадцать',
         16: 'шестнадцать', 17: 'семнадцать', 18: 'восемнадцать', 
         19: 'девятнадцать'}
tens = {2: 'двадцать', 3: 'тридцать', 4: 'сорок',
         5: 'пятдесят', 6: 'шестдесят', 7: 'семдесят',
         8: 'восемдесят', 9: 'девяносто'}
hundreds = {1: 'сто', 2: 'двести', 3: 'триста',
         4: 'четыреста', 5: 'пятьсот', 6: 'шестьсот',
         7: 'семьсот', 8: 'восемьсот', 9: 'девятьсот'}
thousands = {1: 'одна тысяча', 2: 'две тысячи', 3: 'три тысячи',
         4: 'четыре тысячи', 5: 'пять тысяч', 6: 'шесть тысяч',
         7: 'семь тысяч', 8: 'восемь тысяч', 9: 'девять тысяч'}

reference_of_numbers = {4: units, 3: tens, 2: hundreds, 1: thousands}


def output(number):
    number = str(number)
    reference_position = 4 - len(number)
    result = []
    value_of_cikle = ''
    for i in number:
        reference_position += 1
        if len(value_of_cikle) < 1:
            try:
                result.append(reference_of_numbers[int(reference_position)][int(i)])
            except:
                value_of_cikle = i
        else:
            try:
                value_of_cikle = str(value_of_cikle) + str(i)
                result.append(reference_of_numbers[int(reference_position)][int(value_of_cikle)])
            except:
                pass
    if number[-1] == '0':
        result = result[0:-1]
    return ' '.join(result)


def input_validation():
    while True:
        try:
            num = int(input('Please input a number: '))
            try:
                if 0 <= num <= 9999:
                    return num
                else:
                    print('ValueError')
            except UnboundLocalError:
                print('UnboundLocalError')

        except ValueError:
            print('It must be only digits')


def main():
    number_to_output = input_validation()
    print(output(number_to_output))


if __name__ == '__main__':
    main()

